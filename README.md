# smart_school

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### 项目说明

```
//https://cli.vuejs.org/zh/guide/   vue/cli 文档

//如果希望支持 --modern 现代浏览器模式。需要服务器返回响应头 “Access-Control-Allow-Origin: * ”
```

```
package.json

"ASSETS_PATH":"/smartsecurity/wx", //打包的静态资源文件夹

"API_PATH":"/smartsecurity/test",  //打包的API 公用路径

"MOCK_PATH": "/MOCK",   //需要mock的地址
```

---

http {
    server {
            server_name example.com;
            //转发 api 请求
            location /api/smartsecurity/wx/ {
                    proxy_pass http://example.com:protmail/;
            }
    }
}
