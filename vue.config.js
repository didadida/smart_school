let path = require("path");
let fs = require("fs");
const merge = require('webpack-merge');
const tsImportPluginFactory = require('ts-import-plugin');
const package = require('./package.json')
const apiMocker = require("mocker-api")
const FileManagerPlugin = require('filemanager-webpack-plugin');
/**
 * 定义环境变量。
 * 1.只有以 VUE_APP_ 开头的变量会被 webpack.DefinePlugin 静态嵌入到客户端侧的包中。
 * 在vue.config.js中通过process.env.VUE_APP_SECRET = 123写入.在项目中process.env.VUE_APP_VERSION读取
 * 2.通过更改 chainWebpack 配置API_PATH环境变量，在项目中通过 API_PATH 读取
 * config.plugin('define').tap(([options = {}]) => {
        return [{
            ...options,
            API_PATH: JSON.stringify(API_PATH)
        }]
    })
 */
const chunkCache = {
    cacheGroups: {
        // vendor: {
        //     chunks: "all",
        //     test: /[\\/]node_modules[\\/](vue|vant)/,
        //     name: "vendor",
        //     minChunks: 1,
        //     maxInitialRequests: 5,
        //     minSize: 0,
        //     priority: 100,
        // },
        // common: {
        //     chunks: "all",
        //     test: /.*/,
        //     name: "common",
        //     minChunks: 2,
        //     maxInitialRequests: 5,
        //     minSize: 0,
        //     priority: 60
        // },
        styles: {
            name: 'styles',
            test: /\.(sass|scss|less|styl|css)$/,
            chunks: 'all',
            enforce: true,
        },
        // runtimeChunk: {
        //     name: 'manifest'
        // }
    }
}
module.exports = {
    publicPath: package.ASSETS_PATH,
    parallel: false, //防止多线程打包 vant 部分组件样式丢失
    productionSourceMap: false,
    configureWebpack: {
        optimization: {
            splitChunks: process.env.NODE_ENV === "production" ? chunkCache : {}
        },
        plugins:process.env.NODE_ENV === "production"?[
            new FileManagerPlugin({
                onEnd:{
                    copy:[
                        { source: './dist/**/*', destination: './local/smartsecurity/wx' },
                    ]
                }
            })
        ]:[]
    },
    chainWebpack: config => {
        config.resolve.alias
            .set('@', path.resolve(__dirname, 'src'))
            .set('$apis', path.resolve(__dirname, 'src/http'))
            .set('$images', path.resolve(__dirname, 'src/assets/images'))
            .set('$utils', path.resolve(__dirname, 'src/utils'))
            .set('$components', path.resolve(__dirname, 'src/components'))
        config.module
            .rule('ts')
            .use('ts-loader')
            .tap(options => {
                options = merge(options, {
                    transpileOnly: true,
                    getCustomTransformers: () => ({
                        before: [
                            tsImportPluginFactory({
                                libraryName: 'vant',
                                libraryDirectory: 'es',
                                style: name => `${name}/style/less`
                            })
                        ]
                    }),
                    compilerOptions: {
                        module: 'es2015'
                    }
                });
                return options;
            });
        config.module
            .rule('worker')
            .test(/worker\.js$/)
            .use('worker-loader')
            .loader('worker-loader')
            .end()
        config.plugin('define').tap(([options = {}]) => {
            return [{
                ...options,
                API_PATH: JSON.stringify(package.API_PATH),
            }]
        })
        //压缩图片
        config.module
            .rule('images')
            .use('image-webpack-loader')
            .loader('image-webpack-loader')
            .options({
                bypassOnDebug: true
            })
            .end()
    },
    css: {
        loaderOptions: {
            less: {
                modifyVars: {
                    hack: `true; @import "${path.join(__dirname, "src/styles/theme.less")}"`,
                },
            },
            stylus: {
                use: [stylusImportLessVarible()],
            }
        },
    },
    devServer: {
        proxy: {
            "/api": {
                target:"http://af.lailaiche.com",
                // pathRewrite: {'^/api' : ''},
                secure: true,
                changeOrigin: true
            }
        },
        // before(app, server, compiler) {
        //     //自定义中间中，可用来注入cookie 设置请求响应头 代理转发 响应请求 等http操作
        //     //可以通过是否调用next来保证是否向下执行mocker。  app.all = app.get|post|update...
        //     //也可以锚定请求路径而不需要调用next  app.all("test")
        //     //支持正则匹配  app.all("*") app.all(/.*/).
        //     /*
        //     app.all(/.*banner$/, function (req, res, next) {
        //         res.json({ custom: 'response' });
        //     });
        //      */
        //     apiMocker(app, path.resolve('./mocker/index.js'), {
        //         proxy: {
        //             // "/api/(.*)": "http://security.nexusiab.com",
        //             "/api/(.*)": "http://af.lailaiche.com",
        //         },
        //         pathRewrite: {
        //         },
        //     })
        // }
    }

}

const pickFileRegexp = /@import\s+"([\w+(\.\w+)?]+\.less)"/mg
// const ignoreRegexp = /@(?!(extend|block|viewport|page|host|supports|keyframes|font-face|media|import|require))/gm
//stylus 引用less 变量
function stylusImportLessVarible(char = "$") {
    return function (stylus) {
        stylus.str = stylus.str.replace(pickFileRegexp, () => {
            let filePath = path.resolve(stylus.options.filename, "../", RegExp.$1)
            let string = fs.readFileSync(filePath, 'utf8').toString().replace(/@.*;/gm, matched => {
                return matched.replace("@", char).replace(":", "=")
            })
            return "\n" + string + "\n"
        })
    }
}
