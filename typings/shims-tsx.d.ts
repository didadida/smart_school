import Vue, { VNode } from 'vue'
import store from '../src/store'
import { ImagePreview } from "vant";
import {VanToast } from 'vant/types/toast';
declare global {
  namespace JSX {
    // tslint:disable no-empty-interface
    interface Element extends VNode { }
    // tslint:disable no-empty-interface
    interface ElementClass extends Vue { }
    interface IntrinsicElements {
      [elem: string]: any
    }
  }
}


declare module 'vue/types/vue' {
  interface Vue {
    $$store: typeof store,
    $imagePreview: ImagePreview,
    $userInfo: model.UserInfo
    $loading:(message?:string)=>VanToast
  }
}
