declare namespace model {
    interface TableStruct<T = any> {
        pageNum: number
        pageSize: number
        size: number
        total: number
        pages: number
        prePage: number
        nextPage: number
        hasPreviousPage: boolean
        hasNextPage: boolean
        list: (T & { id: any })[]
    }
    interface GetTplRecordListReqBody {
        pageSize: number,
        pageNumber: number,
        self: number,
        status: number,
        stuName: string,
        dateTag: string,
    }
    interface GetTplRecordListItem {
        schoolNameDto: string
        classLevelNameDto: string
        realNameDto: string
        status: number
        temperature: number
        maskEnabled: number
        location: string
    }
    interface GetTplRecordList extends TableStruct {
        list: GetTplRecordListItem[]
    }

    interface Banner { title: string, url: string, imgUrl: string }
    interface Login {
        token: string
        type: 0 | 1 | 2
    }
    interface UserInfo {
        realName: string
        userImage: string
        phone: string
        sex: string
        nickname: string
        schoolName: string
        classLevelName: string
        /**   1老师 2家长 3学生 */
        roletype: 1 | 2 | 3
    }
    interface Timein extends TableStruct<{
        /** 0-老师 1-学生 */
        type: 0 | 1
        inTime: "2020-01-01 10:10:10",
        outTime: "2020-01-01 10:10:10",
        /** 0-迟到 1-正常  */
        inStatus: 0 | 1
        /** 0-迟到 1-正常  */
        outStatus: 0 | 1
        address: string
    }[]> {
        datetime: string
    }
    interface CaptchaImage {
        codeType: 2
        utoken: string
    }
    interface BindPhone {
        temporaryDtoId: string
        /**类型:1：家长登录 其他：学生或老师*/
        type: number
        phone: string
        code: string
        utoken: string
        jcaptcha: string
        /**学号（type=1时为必填参数）*/
        studyNo: string
        /**学生姓名（type=1时为必填参数）*/
        studentName: string
    }
    interface LeaveRecordpayload {
        pageSize: number
        pageNumber: number
        status: number
        stuName: string
        starttime: string | number
        endtime: string | number
    }

    type LeaveRecordList = TableStruct<{
        schoolNameDto: string
        classLevelNameDto: string
        realNameDto: string
        status: number
        createDate: Date
        starttime: Date
        endtime: Date
        reason: string
        rejectReason: string
        remark: string
        imageUrl: string
        imageList: string[]
        longth: string
        typeName: string
    }>

    interface GetTypeList { "value": string, "label": string, "text": string, "index": number }

    interface LeaveAddAndEditPayload {
        id: string
        type: string
        starttime: string
        endtime: string
        longth: number
        reason: string
        imageUrl: string[]
    }

    type GetClassRecordList = TableStruct<{
        schoolNameDto: string
        classLevelNameDto: string
        realNameDto: string
        status: number
        address: number
        type: number
        createDate: string
    }>
    type GetClassRecordList = TableStruct<{
        schoolNameDto: string
        classLevelNameDto: string
        realNameDto: string
        status: number
        address: number
        type: number
        createDate: string
    }>
    interface Scores {
        courseName: string
        score: string
        examName: Date
        uuid: Date
    }
    type GetExamScoreList = TableStruct<{
        schoolNameDto: string
        name: string
        startTime: date
        endTime: date
        scores: Scores[]
    }>
    type GetExamScoreListByWxTeacher = TableStruct<{
        schoolNameDto: string
        name: string
        startTime: Date
        endTime: Date
        scores: Scores[]
    }>

    namespace req {
        interface LeaveReview {
            id: string
            status: 2 | 3
            rejectReason: string
            remark: string
        }
        interface Table {
            pageSize: number
            pageNumber: number
            status: number
            stuName: string
            dateTag: string
            month: string
            year: string
        }
    }
    namespace res {
        interface LeaveLnfo {
            schoolNameDto: string
            classLevelNameDto: string
            realNameDto: string
            status: number
            type: number
            createDate: string
            starttime: string
            endtime: string
            reason: string
            rejectReason: string
            remark: string
            imageUrl: string
            imageList: string[]
            longth: string
            typeName: string
            reviewUser: string
            reviewDate: string

        }
        interface GetAccomRecordList extends TableStruct<{
            id: number
            status: number
            type: number
            address: string
            classLevelNameDto: string
            classLevelUniq: string
            createDate: string
            dateTag: string
            gradeUniq: string
            realNameDto: string
            remark: string
            schoolNameDto: string
            schoolUniq: string
            updateDate: string
            userUniq: string
            uuid: string
            version: string
        }> {

        }

        interface GetExamScoreListByTeacher extends TableStruct<{
            id: number
            status: number
            type: number
            address: string
            classLevelNameDto: string
            classLevelUniq: string
            createDate: string
            dateTag: string
            gradeUniq: string
            realNameDto: string
            remark: string
            schoolNameDto: string
            schoolUniq: string
            updateDate: string
            userUniq: string
            uuid: string
            version: string
        }> {

        }
        interface getExamList extends TableStruct<{
            schoolNameDto: string
            name: string
            startTime: date
            endTime: date
            uuid: string
        }> {

        }
        interface GetAccomRecordStatistics {
            wgcs: number
            wgList: any[]
            weiguics: number
            weiguiList: any[]
        }
        interface TeacherInfo {
            schoolNameDto: string
            classLevelNameDto: string
            realname: string
            sex: number
            identification: string
            introduction: string
            headImageUrl: string
        }
        interface ScoreInquiry extends TableStruct<{
            time: string
            name: string
            id: number
            total: number
            subjects: {
                id: number
                typeName: string
                score: number
            }[]
        }> {

        }

        interface GetClassRecordStatistics {
            skts?: number
            sktsList: SktsItem[]
            cdcs?: number
            cdcsList: SktsItem[]
            ztcs?: number
            ztcsList: SktsItem[]
        }
        interface ItemForTeacher {
            realNameDto: string
            userImage: string
            uuid: string
        }
        interface GetClassRecordStatisticsByTeacher {
            cdxsCount?: number
            cdxsList: ItemForTeacher[]
            ztxsCount?: number
            ztxsList: ItemForTeacher[]
        }
        interface GetAccomRecordStatisticsByTeacher {
            wgxsCount: number
            wgxsList: any[]
            weiguiCount: number
            weigguiList: any[]
        }
        interface SktsItem {
            "id": number
            "schoolUniq": string
            "gradeUniq": string
            "classLevelUniq": string
            "userUniq": string
            "deviceUniq": string
            "address": string
            "status": number
            "type": number
            "dateTag": string
            "createDate": string
            "updateDate": string
            "version": string
            "uuid": string
            "remark": string
            "schoolNameDto": string
            "classLevelNameDto": string
            "realNameDto": string
        }
    }
}


