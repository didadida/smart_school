import { AxiosRequestConfig } from "axios";
declare module '*.png'
declare module '*.jpg'
declare module '*.gif'
declare module '*.styl'
declare module '*.less' {
  const resource: { [key: string]: string };
  export = resource;
}

declare module "worker-loader!*" {
  class WebpackWorker extends Worker {
    constructor();
  }
  export default WebpackWorker;
}

declare module "Axios" {
  interface AxiosRequestConfig {

  }
}
declare module "axios" {
  interface AxiosRequestConfig {
    retry: number
    retryed: number
    retryDelay: number
    requestType: 'json' | 'formData'
  }
}

declare module "compressorjs/dist/compressor.min" {
  import compressorjs from 'compressorjs'
  export default compressorjs
  // export { default } from 'compressorjs'
}

export interface ResponseBody<T = any> {
  code: number
  message: string
  data: T
  type: number
}



