import Vue from 'vue'
import App from './App.vue'
import router, { dynamic } from './router'
import store from './store'
import { Toast, ImagePreview } from "./component.import";

import { getUserInfo } from './http';
import { userInfoKey } from './utils';

// import VConsole from "vconsole";
// new VConsole();

Vue.config.productionTip = false
Vue.prototype.$$store = store
Vue.prototype.$imagePreview = ImagePreview
Vue.prototype.$loading = (message: string) => Toast.loading({ duration: 0, message, forbidClick: true })
async function getInfo() {
  let userInfo = window.localStorage.getItem(userInfoKey)
  if (userInfo) return JSON.parse(userInfo)
  try {
    return await getUserInfo()
  } catch (error) {
    //401 未登录 403 禁止访问
    if (error === 401) return error
    Toast({ duration: 0, message: '拉取用户信息失败，请检查您的网络或退出重试' })
    return 403
  }
}

export default (async function () {
  //在加载vue页面之前，获取用户信息
  let userInfo: model.UserInfo | 403 = await getInfo()
  if (userInfo === 403) return
  store.commit('setUserInfo', userInfo)
  Toast.allowMultiple(true);
  dynamic(userInfo.roletype)
  new Vue({
    router,
    store: store,
    render: h => h(App),
  }).$mount('#app')
})()

