import { VanComponent } from 'vant/types/component'

export function delUnusedKeys(object: Record<string, any>): void {
    for (let key in object) {
        const k = object[key]
        if ((!k && k !== 0) || k === null) {
            delete object[key]
        } else {
            if (Array.isArray(k) && k.length === 0) {
                delete object[key]
            } else if (Object.prototype.toString.call(k) === "[object Object]") {
                if (Object.keys(k).length === 0) {
                    delete object[key]
                } else {
                    delUnusedKeys(k)
                }
            }
        }
    }
}

export const isChineseRegexp: RegExp = /[\u4e00-\u9fa5]{2,}/gm

export const componentsMap = (...components:typeof VanComponent[]): any => {
    let c: Record<string, typeof VanComponent> = {}
    for (const item of components) {
        c[item.name] = item
    }
    return c
}


export function getCookie(cookieName: string) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return null;
}

export const userInfoKey = "userInfo"
