import Vue from 'vue'
import { Divider, Lazyload, Button, List, Cell, CellGroup, Icon, Image, Popup, DatetimePicker, Field, Toast, Empty, Sticky, Tabs, Tab, GoodsActionButton, Dialog, Tabbar, TabbarItem, Calendar, DropdownItem, DropdownMenu, ImagePreview, Row, Col, RadioGroup, Radio, Panel, Collapse, CollapseItem, Uploader, Swipe, SwipeItem, Grid, GridItem, Picker,Loading } from "vant";
[Divider, Lazyload, Button, List, Cell, CellGroup, Icon, Image, Popup, DatetimePicker, Field, Toast, Empty, Sticky, Tabs, Tab, GoodsActionButton, Dialog, Tabbar, TabbarItem, Calendar, DropdownItem, DropdownMenu, ImagePreview, Row, Col, RadioGroup, Radio, Panel, Collapse, CollapseItem, Uploader, Swipe, SwipeItem, Grid, GridItem, Picker,Loading].forEach(item => {
    Vue.use(item);
})
import 'vant/lib/icon/local.css';//引用字体css
import '@/assets/css/iconfont.css';
import "@/styles/app.styl";
import listView from "$components/ListView.vue";

Vue.component('my-list-view', listView);
Vue.directive('visible', {
    inserted(el, binding) {
        el.style.visibility = binding.value ? '' : 'hidden'
    },
    update(el, binding) {
        if (binding.value === binding.oldValue) return;
        el.style.visibility = binding.value ? '' : 'hidden'
    },
});

export { Toast, ImagePreview }
