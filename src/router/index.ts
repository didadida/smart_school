import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import { removeAll } from 'axios'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: '便捷服务',
    component: Home
  },
  {
    path: '/login',
    name: '登录',
    component: () => import(/* webpackChunkName: "login" */ '@/views/Login.vue')
  },
  {
    path: '/healthRecords',
    name: '体温记录',
    component: () => import(/* webpackChunkName: "healthRecords" */ '@/views/HealthRecords.vue')
  },
  {
    path: '/askForLeave',
    name: '请假记录',
    component: () => import(/* webpackChunkName: "askForLeave" */ '@/views/AskForLeave.vue')
  },
  {
    path: '/askForLeaveForm',
    name: '添加请假',
    component: () => import(/* webpackChunkName: "askForLeave" */ '@/views/AskForLeaveForm.vue')
  },
  {
    path: '/askForLeaveForm/:id',
    name: '修改请假',
    component: () => import(/* webpackChunkName: "askForLeave" */ '@/views/AskForLeaveForm.vue')
  },
  // {
  //   path: '/classTimeRecords',
  //   name: '上课考勤',
  //   component: () => import(/* webpackChunkName: "classTimeRecords" */ `@/views/ClassTimeRecords.vue`),
  // },
  // {
  //   path: '/scoreInquiry',
  //   name: '成绩查询',
  //   component: () => import(/* webpackChunkName: "scoreInquiry" */ '@/views/ScoreInquiry.vue')
  // },
  {
    path: '/calendar',
    name: '打卡日历',
    component: () => import(/* webpackChunkName: "calendar" */ '@/views/Calendar.vue')
  },
  {
    path: '/teacherInfo',
    name: '教师信息',
    component: () => import(/* webpackChunkName: "teacherInfo" */ '@/views/TeacherInfo.vue')
  },
  {
    path: '/rules',
    name: '考勤规则',
    component: () => import(/* webpackChunkName: "rules" */ '@/views/rules.vue')
  },
  {
    path: '*',
    name: '出错了!',
    component: () => import(/* webpackChunkName: "404" */ '@/views/404.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
/** 跳转被确认后，清除当前所有请求 */
router.beforeResolve((to, from, next) => {
  removeAll()
  next()
})
router.afterEach(to => {
  document.title = to.name!
})


export function dynamic(roleType: model.UserInfo["roletype"]) {
  router.addRoutes([
    {
      path: '/dormTimeRecords',
      name: '宿舍考勤',
      component: () => {
        if (roleType === 1) {
          return import(/* webpackChunkName: "DormTimeRecordsByTeacher" */ '@/views/DormTimeRecordsByTeacher.vue')
        }
        return import(/* webpackChunkName: "dormTimeRecords" */ '@/views/DormTimeRecords.vue')
      }
    },
    {
      path: '/classTimeRecords',
      name: '上课考勤',
      component: () => {
        if (roleType === 1) {
          return import(/* webpackChunkName: "DormTimeRecordsByTeacher" */ '@/views/ClassTimeRecordsByTeacher.vue')
        }
        return import(/* webpackChunkName: "dormTimeRecords" */ '@/views/ClassTimeRecords.vue')
      }
    },
    {
      path: '/scoreInquiry',
      name: '成绩查询',
      component: () => {
        if (roleType === 1) {
          return import(/* webpackChunkName: "ScoreInquiryByTeacher" */ '@/views/ScoreInquiryByTeacher.vue')
        }
        return import(/* webpackChunkName: "ScoreInquiry" */ '@/views/ScoreInquiry.vue')
      }
    },
  ])
  if (roleType === 1) {
    router.addRoutes([
      {
        path: '/scoreInquiryInfo',
        name: '成绩查询',
        component: () => import(/* webpackChunkName: "scoreInquiryInfo" */ '@/views/scoreInquiryInfo.vue')
      },
    ])
  }
}

export default router
