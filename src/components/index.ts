import Vue, { VueConstructor } from "vue";
import listView from "./ListView.vue";

declare class Component {
    public name: string;
    public install(vue: typeof Vue): void;
}
export { listView }
export default {
    install(vue: typeof Vue) {

    },
}
