import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import { userInfoKey } from '@/utils'

Vue.use(Vuex)
const state: { userInfo: model.UserInfo } = {
  userInfo: {} as any
}
export default new Vuex.Store({
  state,
  mutations: {
    setUserInfo(state, userInfo) {
      window.localStorage.setItem(userInfoKey, JSON.stringify(userInfo))
      state.userInfo = userInfo
      Vue.prototype.$userInfo = userInfo
    }
  },
  actions: {
  },
  modules: {
  }
})
