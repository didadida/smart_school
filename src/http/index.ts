import { common, mock, Methods, AxiosRequestConfigPartial } from "./axios"
interface Request<Req, Res> {
    (data?: Partial<Req>, config?: AxiosRequestConfigPartial): Promise<Res>
    key: string
}
function _factory<Req = any, Res = any>(this: any, method: Methods, path: string): Request<Req, Res> {
    let f: any = (data?: any, config?: any) => this(method, path, data, config)
    f.key = method + "&" + path
    return f
}
const factory = _factory.bind(common)
const mocker = _factory.bind(mock)
/**获取体温记录列表 */
export const getTplRecordList = factory<model.GetTplRecordListReqBody, model.GetTplRecordList>("POST", "/getTplRecordList")
/**获取个人信息 */
export const getUserInfo = factory<never, model.UserInfo>("POST", "/getUserInfo")
/**获取请假记录列表 */
export const leaveRecordList = factory<model.LeaveRecordpayload, model.LeaveRecordList>("POST", "/leave/leaveRecordList")
/**获取短信验证码 */
export const sendSMSCode = factory<{
    phone: number,
    j_captcha: string,
    utoken: string
}>("POST", "/sendSMSCode")
/**绑定用户信息 */
export const bindPhone = factory<model.BindPhone, any>("POST", "/bindPhone")
/**获取请假类型列表 */
export const getTypeList = factory<never, model.GetTypeList[]>("POST", "/leave/getTypeList")
/**添加请假记录 */
export const leaveAdd = factory<model.LeaveAddAndEditPayload, any>("POST", "/leave/add")
/**编辑请假记录 */
export const leaveEdit = factory<model.LeaveAddAndEditPayload, any>("POST", "/leave/edit")
/**撤销删除请假记录 */
export const leaveDelete = factory<{ id: string }, any>("POST", "/leave/delete")
/**审核请假记录 */
export const leaveReview = factory<model.req.LeaveReview, any>("POST", "/leave/review")
/**获取请假记录详情-单条 */
export const leaveLnfo = factory<{ id: string }, model.res.LeaveLnfo>("POST", "/leave/info")
/**上传图片 */
export const uploadFile = factory<{ file: File }, string>("POST", "/uploadFile")
/**轮播图 */
export const banner = factory<never, model.Banner[]>("POST", "/banner")
/**打卡日历-学生/家长 */
export const getAccomRecordList = factory<model.req.Table & { studentUserUniq: string, status: string }, model.res.GetAccomRecordList>("POST", "/getAccomRecordList")
/**获取上课考勤记录列表 */
export const getClassRecordList = factory<model.req.Table & { studentUserUniq: string, status: string }, model.res.GetAccomRecordList>("POST", "/getClassRecordList")

/**学生获取成绩记录列表 */
export const getExamScoreList = factory<model.req.Table & {
    name: string
    month: string
    year: string
}, model.GetExamScoreList>("POST", "/score/getExamScoreList")
/**老师.获取指定考试场次的所有学生的成绩记录列表 */
export const getExamScoreListByTeacher = factory<model.req.Table & {
    examinationUniq: string,
    realname: string,
}, model.res.GetExamScoreListByTeacher>("POST", "/score/getExamScoreListByTeacher")
/**老师获取所有考试场次  - 老师界面列表*/
export const getExamList = factory<model.req.Table & {
    name: string,
}, model.res.getExamList>("POST", "/score/getExamList")

/**上课考勤统计-学生/家长/ */
export const getClassRecordStatistics = factory<{ month: string }, model.res.GetClassRecordStatistics>("POST", "/getClassRecordStatistics")
/**上课考勤统计-老师*/
export const getClassRecordStatisticsByTeacher = factory<{ month: string, dateTag: string }, model.res.GetClassRecordStatisticsByTeacher>("POST", "/getClassRecordStatisticsByTeacher")
/**宿舍考勤统计-学生/家长 */
export const getAccomRecordStatistics = factory<{ month: string }, model.res.GetAccomRecordStatistics>("POST", "/getAccomRecordStatistics")
/**宿舍考勤统计-老师 */
export const getAccomRecordStatisticsByTeacher = factory<{ month: string }, model.res.GetAccomRecordStatisticsByTeacher>("POST", "/getAccomRecordStatisticsByTeacher")
/**教师信息 */
export const getTeacherInfo = factory<never, model.res.TeacherInfo>("POST", "/getTeacherInfo")
/** 考勤规则 */
export const getSchoolRulesInfo = factory<{ type: number }, { ruleName: string, ruleContent: string }>("POST", "/getSchoolRulesInfo")



export { abort } from "axios"



