//@ts-nocheck
import Axios, { AxiosResponse, AxiosError, AxiosRequestConfig } from "axios";
import { ResponseBody } from 'typings/axios';
import { Toast } from 'vant';
export const baseURL = API_PATH + "/wx"
Axios.defaults.setting({
    requestType: 'formData',
    baseURL,
    retryDelay: 3000,
    retry: 3,
    timeout: 20000,
    validateStatus(){return true},
})

Axios.interceptors.response.use((resp: AxiosResponse<ResponseBody>) => {
    if (resp.status === 200 && resp.data.code === 200) {
        return resp.data.data
    }
    return Promise.reject(resp)
})

Axios.interceptors.response.use(undefined, (err: string | AxiosError | AxiosResponse<ResponseBody>) => {
    if (typeof err === "string") Toast('网络错误!')
    else {
        if ('status' in err) {
            //服务器响应错误
            if (err.status === 200) {
                //接口层错误
                if (err.data.code === 403) {
                    //如果没有temporaryDtoId参数需要去授权页
                    if (!window.location.search.includes("temporaryDtoId")) {
                        window.location.href = window.location.origin + process.env.BASE_URL.replace("wx", "wxlogin/auth") + "?callback=" + encodeURIComponent(window.location.href)
                    }
                    // //把当前的URL作为参数带到授权页
                    return Promise.reject(401)
                }
                //接口错误
                else if (err.data.message) Toast(err.data.message)
            } else Toast(`网络错误：` + err.status)
        } else {
            if (Axios.isCancel(err)) {
                //客户端取消
                return Promise.reject(err)
            } else if (err.code === "ECONNABORTED") {
                Toast('请求超时!')
            } else Toast('网络错误!')
        }
    }
    return Promise.reject(err)
})

export type AxiosRequestConfigPartial = Partial<AxiosRequestConfig>
export type Methods = "GET" | "POST" | "PATCH" | "DELETE" | "HEAD" | "PUT"
//不能使用箭头函数，否则没有this无法绑定
function _common(method: Methods, url: string, data?: any, config: AxiosRequestConfigPartial = {}): Promise<any> {
    config.params = method === "get" ? data : {};
    return new Promise((resolve, reject) => {
        //如果是取消请求跑出的错误，不继续向下冒泡。
        this(Object.assign({ url, method, data }, config))
            .then(resolve)
            .catch(err => !Axios.isCancel(err) && reject(err))
    })
}
export const common = _common.bind(Axios)
export const mock = _common.bind(Axios.extend({ baseURL: "" }))
export const setHeader = (key: string, value: any) => Axios.defaults.headers[key] = value
