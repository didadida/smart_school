const delay = require('mocker-api/lib/delay');
const json = require("./data.json")
let data = new Proxy(json, {
  get(target, prop, receiver) {
    return {
      code: 200,
      message: "",
      data: target[prop]
    }
  }
})
let table = (data) => {
  data.data = {
    pageNum:1,
    pageSize: 20,
    size: 1000,
    total: 1000,
    pages: 10,
    prePage: 0,
    nextPage: 2,
    hasPreviousPage: false,
    hasNextPage: false,
    list: data.data,
  }
  return data
}
module.exports = delay({
  "POST /scoreInquiry"(req, res) {
    res.json(table(data.scoreInquiry))
  },
  "POST /timein"(req, res) {
    res.json(table(data.timein))
  }
}, 1000)
